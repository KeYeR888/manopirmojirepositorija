create table A1 
(
	A1 int primary key auto_increment,
    A11 varchar(5)
);

create table A2 (
	A2 int primary key auto_increment,
    A22 int,
    A1 int,
    foreign key (A1) references A1(A1)
		on delete cascade
);

create table B1(
	B1 int primary key auto_increment,
    B11 varchar(5),
    A2 int not null,
    A1 int,
    foreign key (A2) references A2(A2)
		on delete set null,
	foreign key (A1) references A1(A1)
		on delete cascade
);